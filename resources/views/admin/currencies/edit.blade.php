@extends('admin.layouts.default')

@section('header')
   
@stop


@section('content')

<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">Default Box Example</h3>
    <div class="box-tools pull-right">
      <!-- Buttons, labels, and many other things can be placed here! -->
      <!-- Here is a label for example -->
        <button class="btn btn-block btn-success btn-flat" type="button">Go Back To List</button>
    </div><!-- /.box-tools -->
  </div><!-- /.box-header -->
  <div class="box-body">

       <div class="alert alert-success">
		  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		  <strong>Success!</strong> Indicates a successful or positive action.
	   </div>
     <div class="clearfix"></div>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
           <strong>Danger!</strong> Indicates a dangerous or potentially negative action.
        </div>
   <div class="clearfix"></div>
     <div class="row">
     	<div class="col-md-6">
     		 <div class="form-group">
                 <label>Currency Name</label>
                
                   {!! Form::hidden('id', $ecom_currencies->id, ["id"=>'id']) !!}

                   <input type="hidden" id="token" name="token" value="{{csrf_token() }}" />
          
                 

                   <input class="form-control" type="text" id="currency_name" value="{{ $ecom_currencies->currency_name }}" placeholder="Enter ..." />
             </div>
     	</div>
        <div class="col-md-6">
        	 <div class="form-group">
                 <label>Currency Symbol</label>
                   <input class="form-control" value="{{ $ecom_currencies->currency_symbol }}" id="currency_symbol" type="text" placeholder="Enter ..." />
             </div>
        </div>
        
     </div>
     <div class="row">
     	<div class="col-md-6">
     		<div class="checkbox">
					<label>
           @if($ecom_currencies->is_active ==0)
					<input type="checkbox" id="is_active">
					 Is Active
          @elseif($ecom_currencies->is_active ==1)
             <input type="checkbox" checked="checked" id="is_active">
           Is Active
           @endif
					</label>
			</div>

     	</div>
     </div>
  
  </div><!-- /.box-body -->
  <div class="box-footer">
      <button id="btnSaveCurrency" class="btn btn-info pull-right" type="submit">Save Currency</button>
  </div><!-- box-footer -->
</div><!-- /.box -->
  
@stop


@section('javascript')
   <script type="text/javascript">
      $(document).ready(function(){

      	$("#btnSaveCurrency").click(function(){

      		        var token =$("#token").val();
      	            var currency_name=$("#currency_name").val();
      	            var currency_symbol= $("#currency_symbol").val();
                    var id= $("#id").val();
      	            /* tarnary logic */ 
      	            var is_active= $("#is_active").is(":checked") == true ? 1: 0;

      	           // console.log(data);
                       
      	                 	        
			          
			        $.ajax({
				           url: 'currencyedit',
				           type: 'PUT',
				           dataType: 'json',
				           data: {
                             '_token': token,
                             'id':id,
                             'currency_name':currency_name,
                             'currency_symbol':currency_symbol,
                             'is_active':is_active
				           },
				           success: function(data, textStatus, xhr) {
				             console.log(data);
				           

				             //jQuery("#journal_id").val(data.journal_id);
				             
				           },
				           error: function(xhr, textStatus, errorThrown) {
				             //called when there is an error
				           }
                    });

                	
	    });

    

      });
   </script>

@stop