@extends('admin.layouts.default')

@section('header')
  <link rel="stylesheet" href="{{asset('admin/components/plugins/datatables/dataTables.bootstrap.css')}}">

  <script src="{{asset('admin/components/plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('admin/components/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
@stop


@section('content')



<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">Default Box Example</h3>
    <div class="box-tools pull-right">
      <!-- Buttons, labels, and many other things can be placed here! -->
      <!-- Here is a label for example -->
      <span class="label label-primary">Label</span>
    </div><!-- /.box-tools -->
  </div><!-- /.box-header -->
  <div class="box-body">
    <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Currency Name</th>
                  <th>Currency Symbol</th>
                  <th>is_active</th>
                  <th>Action</th>
                  
                </tr>
                </thead>
                <tbody>
                @foreach($ecom_currencies as $cur)
                     <tr id="{{$cur->id}}">
                     	<td>{{ $cur->currency_name }}</td>
                     	<td>{{ $cur->currency_symbol }}</td>
                     	<td>{{ $cur->is_active }}</td>
                     	<td><a href="edit/{{$cur->id}}">Edit</a>&nbsp;<button value="{{$cur->id}},tr_{{$cur->id}},{{ csrf_token() }}" onclick="deletes(this.value);">Delete</button></td>
                 
                     </tr>
   
                 @endforeach
             
             
                </tbody>
                <tfoot>
                <tr>
                 <th>Currency Name</th>
                  <th>Currency Symbol</th>
                  <th>is_active</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>


  </div><!-- /.box-body -->
  <div class="box-footer">
    The footer of the box
  </div><!-- box-footer -->
</div><!-- /.box -->
 
@stop

@section('javascript')
  <script type="text/javascript">
     $(document).ready(function(){
            $("#example1").DataTable();
     });

      function deletes(val){
                 var btnValue = val;

                 var splitValue = btnValue.split(',');

                 $.ajax({
                   url: 'delete',
                   type: 'DELETE',
                   dataType: 'json',
                   data: {
                             '_token': splitValue[2],
                              'id':splitValue[0]
                   },
                   success: function(data, textStatus, xhr) {
                     console.log(data);
                     console.log(splitValue[1]);
                     //$("#"+splitValue[1]).hide(slow);
                     //jQuery("#journal_id").val(data.journal_id);
                     
                   },
                   error: function(xhr, textStatus, errorThrown) {
                     //called when there is an error
                   }
                    });
               
                 //console.log(splitValue);
      }
  </script>
@stop