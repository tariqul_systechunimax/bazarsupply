<div class="container">
<div class="menu-shadow">
<ul class="menu">
<li>
<a href="vegetables.php">Vegitables</a>
</li>
<li>
<a href="#">Meat & Fish</a>
<ul>
<li>
<a href="#">Meat</a>
</li>
<li>
<a href="#">Fish</a>
</li>
<li>
<a href="#">Snacks</a>
</li>
<li>
<a href="#" class="parent">Fruits</a><ul>
<li>
<a href="#">Diary</a>
</li>
<li>
<a href="#">Cooking</a>
</li>
</ul>
</li>
<li>
<a href="#">Fruits</a>
</li>
</ul>
</li>
<li>
<a href="#">Beverages</a>
</li>
<li>
<a href="#">Breakfast</a>
</li>
<li>
<a href="#">Bread & Bakery</a>
</li>
<li>
<a href="#">Frozen Food</a>
</li>
<li>
<a href="#">Dairy</a>
</li>
<li>
<a href="#">Cooking</a>
</li>
<li>
<a href="#">Snacks</a>
</li>
</ul>
<div class="box-cart">
<div id="cart" class="cart">
<button type="button" data-toggle="dropdown" data-loading-text="Loading..." class="dropdown-toggle">
<i class="fa fa-shopping-cart"></i>
<strong>Cart:</strong>
<span id="cart-total" class="cart-total">0 item(s) - $0.00</span>
<span id="cart-total2" class="cart-total2">0</span> <span id="cart-total3" class="cart-total3">0 item(s)</span> </button>
<ul class="dropdown-menu pull-right">
<li>
<p class="text-center">Your shopping cart is empty!</p>
</li>
</ul>
</div>
</div>
</div>
</div>