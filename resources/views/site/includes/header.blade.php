<div class="col-sm-12">
<div id="logo" class="logo">
<a href="index.php"><img src="images/logo.png" title="Food Shop" alt="Food Shop" class="img-responsive"/></a>
</div>
<div class="box-right">
<div class="box-language">
<form action="" method="post" enctype="multipart/form-data" id="language">
<div class="btn-group pull-right">
<span class="dropdown-toggle" data-toggle="dropdown">
<img class="hidden" src="images/gb.png" alt="English" title="English">
en <span class="hidden-xs hidden-sm hidden-md hidden">Language</span></span>
<ul class="dropdown-menu pull-right">
<li><a href="en"><img class="hidden" src="images/gb.png" alt="English" title="English"/> English</a></li>
<li><a href="de"><img class="hidden" src="images/de.png" alt="Deutsch" title="Deutsch"/> Deutsch</a></li>
<li><a href="ru"><img class="hidden" src="images/ru.png" alt="Русский" title="Русский"/> Русский</a></li>
</ul>
</div>
<input type="hidden" name="code" value=""/>
<input type="hidden" name="redirect" value="http://livedemo00.template-help.com/opencart_53122/index.php?route=common/home"/>
</form>
</div>
<div class="box-currency">
<form action="http://livedemo00.template-help.com/opencart_53122/index.php?route=common/currency/currency" method="post" enctype="multipart/form-data" id="currency">
<div class="btn-group">
<span class="dropdown-toggle" data-toggle="dropdown">
$ <span class="hidden-xs hidden-sm hidden-md hidden">Currency</span>
</span>
<ul class="dropdown-menu  pull-right">
<li><button class="currency-select" type="button" name="EUR">€ Euro</button></li>
<li><button class="currency-select" type="button" name="GBP">£ Pound Sterling</button></li>
<li><button class="currency-select" type="button" name="USD">$ US Dollar</button></li>
</ul>
</div>
<input type="hidden" name="code" value=""/>
<input type="hidden" name="redirect" value="http://livedemo00.template-help.com/opencart_53122/index.php?route=common/home"/>
</form>
</div>
</div>
</div>