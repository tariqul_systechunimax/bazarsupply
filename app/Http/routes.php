<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/admin/currency/index','Admin\CurrencyController@index');
Route::get('/site/index','Site\SiteController@index');
Route::post('/admin/currencysave','Admin\CurrencyController@store');
Route::get('admin/currency/list','Admin\CurrencyController@viewcurrency');
Route::get('admin/currency/edit/{id}','Admin\CurrencyController@edit');
Route::put('admin/currency/edit/currencyedit','Admin\CurrencyController@update');
Route::delete('admin/currency/delete', 'Admin\CurrencyController@destroy');