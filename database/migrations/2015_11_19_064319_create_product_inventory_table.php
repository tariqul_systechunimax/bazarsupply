<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ecom_product_inventories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->nullable;
            $table->decimal('avilable_product_quantity',11,2)->nullable;
            $table->decimal('total_store_quantity',11,2)->nullable;
            $table->decimal('sold_quantity',11,2)->nullable;
            $table->date('stockin_date')->nullable;
            $table->date('stokeout_date')->nullable;
            $table->date('product_avilability_date')->nullable;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ecom_product_inventories');
    }
}
