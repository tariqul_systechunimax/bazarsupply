$(document).ready(function () {
    /**************swipe menu***************/
    $('#page').click(function () {
        if ($(this).parents('body').hasClass('ind')) {
            $(this).parents('body').removeClass('ind');
            return false
        }
    })
    $('.swipe-control').click(function () {
        if ($(this).parents('body').hasClass('ind')) {
            $(this).parents('body').removeClass('ind');
            $(this).removeClass('active');
            return false
        }
        else {
            $(this).parents('body').addClass('ind');
            $(this).addClass('active');
            return false
        }
    })
///****************BACK TO TOP*********************/
    $().UItoTop({easingType: 'easeOutQuart'});


    /**************lazy load***************/
    $("img.lazy").unveil(1, function () {
        $(this).load(function () {
            $(this).animate({'opacity': 1}, 700);
        });
    });

    /************product gallery on product page***********/
    if ($("#gallery_zoom").length) {
        $("#gallery_zoom").elevateZoom({
            gallery: 'image-additional',
            cursor: 'pointer',
            zoomType: 'inner',
            galleryActiveClass: 'active',
            imageCrossfade: true
        });
//pass the images to Fancybox
        $("#gallery_zoom").bind("click", function (e) {
            var ez = $('#gallery_zoom').data('elevateZoom');
            $.fancybox(ez.getGalleryList());
            return false;
        });
    }
    if ($('#image-additional').length) {
        $('#image-additional').bxSlider({
            mode: 'vertical',
            pager: false,
            controls: true,
            slideMargin: 13,
            minSlides: 6,
            maxSlides: 6,
            slideWidth: 88,
            nextText: '<i class="fa fa-chevron-down"></i>',
            prevText: '<i class="fa fa-chevron-up"></i>',
            infiniteLoop: false,
            adaptiveHeight: true,
            moveSlides: 1
        });
    }
    if ($('#gallery').length) {
        $('#gallery').bxSlider({
            pager: false,
            controls: true,
            minSlides: 1,
            maxSlides: 1,
            infiniteLoop: false,
            moveSlides: 1
        });
    }

    (function ($) {
        $.fn.equalHeights = function (minHeight, maxHeight) {
            tallest = (minHeight) ? minHeight : 0;
            this.each(function () {
                if ($(this).height() > tallest) {
                    tallest = $(this).height()
                }
            });
            if ((maxHeight) && tallest > maxHeight)tallest = maxHeight;
            return this.each(function () {
                $(this).height(tallest)
            })
        }
    })($)

    /***********CATEGORY DROP DOWN****************/
    $("#menu-icon").on("click", function () {
        $("#menu-gadget .menu").slideToggle();
        $(this).toggleClass("active");
    });

    $('#menu-gadget .menu').find('li>ul').before('<i class="fa fa-angle-down"></i>');
    $('#menu-gadget .menu li i').on("click", function () {
        if ($(this).hasClass('fa-angle-up')) {
            $(this).removeClass('fa-angle-up').parent('li').find('> ul').slideToggle();
        }
        else {
            $(this).addClass('fa-angle-up').parent('li').find('> ul').slideToggle();
        }
    });

    if ($('.tab-heading').length){
        alert('1');
    }


    /************************* RELATED PRODUCTS************************************/
    if ($('.related-slider').length) {
        $('.related-slider').owlCarousel({
            smartSpeed: 450,
            dots: false,
            nav: true,
            loop: true,
            margin: 30,
            navClass: ['owl-prev fa fa-angle-left', 'owl-next fa fa-angle-right'],
            responsive: {
                0: {items: 1},
                768: {items: 2},
                992: {items: 3},
                1199: {items: 4}
            }
        });
    }

    if ($('#google-map').length) {
        $('#google-map').googleMap({
            styles: [
                {
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "gamma": 0.8
                        },
                        {
                            "lightness": 4
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "landscape.natural",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#5dff00"
                        },
                        {
                            "gamma": 4.97
                        },
                        {
                            "lightness": -5
                        },
                        {
                            "saturation": 100
                        }
                    ]
                }
            ]
        });
    }
});

var flag = true;
function respResize() {
    var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

    if ($('aside').length){
        var leftColumn = $('aside');
    }else{
        return false;
    }

    if (width > 767) {
        if (!flag) {
            flag = true;
            leftColumn.insertBefore('#content');
            $('.col-sm-3 .box-heading').unbind("click");

            $('.col-sm-3 .box-content').each(function () {
                if ($(this).is(":hidden")){
                    $(this).slideToggle();
                }
            })
        }
    } else {
        if (flag) {
            flag = false;
            leftColumn.insertAfter('#content');

            $('.col-sm-3 .box-content').each(function () {
                if (!$(this).is(":hidden")){
                    $(this).parent().find('.box-heading').addClass('active');
                }
            });

            $('.col-sm-3 .box-heading').bind("click", function () {
                if ($(this).hasClass('active')) {
                    $(this).removeClass('active').parent().find('.box-content').slideToggle();
                }
                else {
                    $(this).addClass('active');
                    $(this).parent().find('.box-content').slideToggle();
                }
            })

            //$('.tab-heading').append('<i class="fa fa-plus-circle"></i>');
            //$('.tab-heading').on("click", function () {
            //    if ($(this).find('i').hasClass('fa-minus-circle')) {
            //        $(this).find('i').removeClass('fa-minus-circle').parents('.tabs').find('.tab-content').slideToggle();
            //    }
            //    else {
            //        $(this).find('i').addClass('fa-minus-circle').parents('.tabs').find('.tab-content').slideToggle();
            //    }
            //})
        }
    }
}



$(window).resize(function () {
    clearTimeout(this.id);
    this.id = setTimeout(respResize, 500);
});


$(window).load(function () {
    $('#tm_menu .menu').superfish();

    if ($("#content .product-grid .name").length) {
        $("#content .product-grid .name").equalHeights()
    }
    if ($(".footer_box").length) {
        $(".footer_box").equalHeights();
    }
})


var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent);

/***********************************/
if (!isMobile) {
    /***********************Green Sock*******************************/

    $(document).ready(function () {
        var stickMenu = false;
        var docWidth = $('body').find('.container').width();
            // init controller
            controller = new ScrollMagic();

    })


    function listBlocksAnimate(block, element, row, offset, difEffect) {
            if ($(block).length) {
                var i = 0;
                var j = row;
                var k = 1;
                var effect = -1;

                $(element).each(function () {
                    i++;

                    if (i > j) {
                        j += row;
                        k = i;
                        effect = effect * (-1);
                    }

                    if (effect == -1 && difEffect == true) {
                        ef = TweenMax.from(element + ':nth-child(' + i + ')', 0.5, {
                            left: -1 * 200 - i * 300 + "px",
                            alpha: 0,
                            ease: Power1.easeOut
                        })

                    } else {
                        ef = TweenMax.from(element + ':nth-child(' + i + ')', 0.5, {
                            right: -1 * 200 - i * 300 + "px",
                            alpha: 0,
                            ease: Power1.easeOut
                        })
                    }

                    var scene_new = new ScrollScene({
                        triggerElement: element + ':nth-child(' + k + ')',
                        offset: offset,
                    }).setTween(ef)
                        .addTo(controller)
                        .reverse(false);
                });
            }
    }

    function listTabsAnimate(element) {
            if ($(element).length) {
                TweenMax.staggerFromTo(element, 0.3, {alpha: 0, rotationY: -90, ease: Power1.easeOut}, {
                    alpha: 1,
                    rotationY: 0,
                    ease: Power1.easeOut
                }, 0.1);
            }
    }

    $(window).load(function () {
            if ($(".fluid_container").length) {
                var welcome = new TimelineMax();

                welcome.from(".fluid_container h2", 0.5, {top: -300, autoAlpha: 0})
                    .from(".fluid_container h4", 0.5, {bottom: -300, autoAlpha: 0});

                var scene_welcome = new ScrollScene({
                    triggerElement: ".fluid_container",
                    offset: -100
                }).setTween(welcome).addTo(controller).reverse(false);
            }
    });
}


