<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]>
<html dir="ltr" lang="en" class="ie8"><![endif]-->
<!--[if IE 9 ]>
<html dir="ltr" lang="en" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="ltr" lang="en">
<!--<![endif]-->
<head>
<meta charset="UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript">
//<![CDATA[
try{if (!window.CloudFlare) {var CloudFlare=[{verbose:0,p:0,byc:0,owlid:"cf",bag2:1,mirage2:0,oracle:0,paths:{cloudflare:"/cdn-cgi/nexp/dok3v=1613a3a185/"},atok:"39c92a14ce8d138976e99cb84d848f88",petok:"4c3983695744843353d47cded8e62ca75197f440-1425743866-86400",zone:"template-help.com",rocket:"0",apps:{"abetterbrowser":{"ie":"7"},"ga_key":{"ua":"UA-7078796-5","ga_bs":"2"}}}];!function(a,b){a=document.createElement("script"),b=document.getElementsByTagName("script")[0],a.async=!0,a.src="//ajax.cloudflare.com/cdn-cgi/nexp/dok3v=919620257c/cloudflare.min.js",b.parentNode.insertBefore(a,b)}()}}catch(e){};
//]]>
</script>
<script type="text/javascript">var NREUMQ=NREUMQ||[];NREUMQ.push(["mark","firstbyte",new Date().getTime()]);</script><title>Food Shop</title>
<base href="http://bazarsupply.com/"/>
<meta name="description" content="Food Shop"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link href="images/favicon.png" rel="icon"/>
 

 
 
<script src="{{asset('site/components/js/jquery-2.1.1.min.js')}}" type="text/javascript')}}"></script>
<link href="{{asset('site/components/css/bootstrap.min.css')}}" rel="stylesheet" media="screen"/>
<script src="{{asset('site/components/js/bootstrap.min.js')}}" type="text/javascript"></script>
<link href="{{asset('site/components/css/camera.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('site/components/css/jquery.fancybox.css')}}" media="screen"/>
<link href="{{asset('site/components/css/owl-carousel.css')}}" rel="stylesheet">
<link href="{{asset('site/components/css/photoswipe.css')}}" rel="stylesheet">
<link href="{{asset('site/components/css/jquery.bxslider.css')}}" rel="stylesheet">
<link href="{{asset('site/components/css/stylesheet.css')}}" rel="stylesheet">
 
<script src="{{asset('site/components/js/camera.js')}}" type="text/javascript"></script>
<script src="{{asset('site/components/js/owl.carousel.min.js')}}" type="text/javascript"></script>
<script src="{{asset('site/components/js/tmstickup.js')}}" type="text/javascript"></script>
<script src="{{asset('site/components/js/common.js')}}" type="text/javascript"></script>
<script src="{{asset('site/components/js/jquery.unveil.js')}}" type="text/javascript"></script>
<script src="{{asset('site/components/js/jquery.bxslider.js')}}" type="text/javascript"></script>
<script src="{{asset('site/components/js/jquery.fancybox.pack.js')}}"></script>
<script src="{{asset('site/components/js/jquery.elevatezoom.js')}}" type="text/javascript"></script>
<script src="{{asset('site/components/js/superfish.js')}}" type="text/javascript"></script>
<script src="{{asset('site/components/js/jquery.ui.totop.js')}}"></script>
<script src="{{asset('site/components/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('site/components/js/jquery.vide.js')}}"></script>
<script src="//maps.google.com/maps/api/js?sensor=false')}}"></script>
<script src="{{asset('site/components/js/jquery.rd-google-map.js')}}"></script>
 
<script src="{{asset('site/components/js/jquery.gsap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('site/components/js/TimelineMax.min.js')}}" type="text/javascript"></script>
<script src="{{asset('site/components/js/TweenMax.min.js')}}" type="text/javascript"></script>
<script src="{{asset('site/components/js/jquery.scrollmagic.min.js')}}" type="text/javascript"></script>
 
<script src="{{asset('site/components/js/klass.min.js')}}" type="text/javascript"></script>
<script src="{{asset('site/components/js/code.photoswipe.jquery-3.0.5.js')}}" type="text/javascript"></script>
<script src="{{asset('site/components/js/code.photoswipe-3.0.5.min.js')}}" type="text/javascript"></script>
<!--[if lt IE 9]>
    <div style='clear:both;height:59px;padding:0 15px 0 15px;position:relative;z-index:10000;text-align:center;'>
        <a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img
                src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0"
                height="42" width="820"
                alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."/>
        </a>
    </div><![endif]-->
<script type="text/javascript">
/* <![CDATA[ */
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-7078796-5']);
_gaq.push(['_trackPageview']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

(function(b){(function(a){"__CF"in b&&"DJS"in b.__CF?b.__CF.DJS.push(a):"addEventListener"in b?b.addEventListener("load",a,!1):b.attachEvent("onload",a)})(function(){"FB"in b&&"Event"in FB&&"subscribe"in FB.Event&&(FB.Event.subscribe("edge.create",function(a){_gaq.push(["_trackSocial","facebook","like",a])}),FB.Event.subscribe("edge.remove",function(a){_gaq.push(["_trackSocial","facebook","unlike",a])}),FB.Event.subscribe("message.send",function(a){_gaq.push(["_trackSocial","facebook","send",a])}));"twttr"in b&&"events"in twttr&&"bind"in twttr.events&&twttr.events.bind("tweet",function(a){if(a){var b;if(a.target&&a.target.nodeName=="IFRAME")a:{if(a=a.target.src){a=a.split("#")[0].match(/[^?=&]+=([^&]*)?/g);b=0;for(var c;c=a[b];++b)if(c.indexOf("url")===0){b=unescape(c.split("=")[1]);break a}}b=void 0}_gaq.push(["_trackSocial","twitter","tweet",b])}})})})(window);
/* ]]> */
</script>
</head>
<body class="common-home">
 
<!--<div class="swipe">
<div class="swipe-menu">
<ul>
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=account/account" title="My Account"><i class="fa fa-user"></i>
<span>My Account</span></a>
</li>
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=account/register"><i class="fa fa-user"></i> Register</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=account/login"><i class="fa fa-lock"></i>Login</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=account/wishlist" id="wishlist-total2" title="Wish List (0)"><i class="fa fa-heart"></i> <span>Wish List (0)</span></a>
</li>
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=checkout/cart" title="Shopping Cart"><i class="fa fa-shopping-cart"></i> <span>Shopping Cart</span></a>
</li>
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=checkout/checkout" title="Checkout"><i class="fa fa-share"></i>
<span>Checkout</span></a>
</li>
</ul>
<ul class="foot">
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=information/information&amp;information_id=4">About Us</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=information/information&amp;information_id=6">Delivery Information</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=information/information&amp;information_id=3">Privacy Policy</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=information/information&amp;information_id=5">Terms &amp; Conditions</a>
</li>
</ul>
<ul class="foot foot-1">
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=information/contact">Contact Us</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=account/return/insert">Returns</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=information/sitemap">Site Map</a>
</li>
</ul>
<ul class="foot foot-2">
<li>
<a href="#">Brands</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=account/voucher">Gift Vouchers</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=affiliate/account">Affiliates</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/special">Specials</a>
</li>
</ul>
<ul class="foot foot-3">
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=account/order">Order History</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=account/newsletter">Newsletter</a>
</li>
</ul>
</div>
</div>-->
<div id="page">
<div class="shadow"></div>
<div class="toprow-1">
<a class="swipe-control" href="#"><i class="fa fa-align-justify"></i></a>
</div>
<header class="header">
<!--<nav id="top" class="top_panel">
<div class="container">
<div class="row">
<div class="col-sm-12">
<div id="top-links" class="nav">
<ul class="list-inline">
<li class="first">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=common/home"><i class="visible-sm fa fa-home"></i><span class="hidden-sm">Home</span></a>
</li>
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=account/wishlist" id="wishlist-total" title="Wish List (0)"><i class="visible-sm fa fa-heart"></i> <span class="hidden-sm">Wish List (0)</span></a>
</li>
<li class="dropdown">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=account/account" title="My Account" class="dropdown-toggle" data-toggle="dropdown"><i class="visible-sm fa fa-user"></i>
<span class="hidden-sm">My Account</span> <span class="caret hidden-sm"></span></a>
<ul class="dropdown-menu dropdown-menu-left">
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=account/register">Register</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=account/login">Login</a>
</li>
</ul>
</li>
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=checkout/cart" title="Shopping Cart">
<i class="visible-sm fa fa-shopping-cart"></i> <span class="hidden-sm">Shopping Cart</span></a>
</li>
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=checkout/checkout" title="Checkout"><i class="visible-sm fa fa-share"></i> <span class="hidden-sm">Checkout</span></a>
</li>
</ul>
<div id="search" class="search">
<input type="text" name="search" placeholder="Search"/>
<button type="button" class="button-search"></button>
</div> </div>
</div>
</div>
</div>
</nav>-->
<div class="container">
<div class="row">
@include ('site.includes.header')
</div>
</div>
<script type="text/javascript">
            jQuery(window).load(function () {
                ('#tm_menu').TMStickUp({})
            });
        </script>
<div id="tm_menu" class="nav__primary">
@include ('site.includes.menu')
</div>
<div class="container">
@include ('site.includes.responsive')
</div>
</header>
<div class="header_modules"><script>
	jQuery(function(){
		jQuery('#camera_wrap_0').camera({
			navigation: true,
			playPause: false,
			thumbnails: false,
			navigationHover: false,
			barPosition: 'top',
			loader: false,
			time: 3000,
			transPeriod:800,
			alignment: 'center',
			autoAdvance: true,
			mobileAutoAdvance: true,
			barDirection: 'leftToRight', 
			barPosition: 'bottom',
			easing: 'easeInOutExpo',
			fx: 'simpleFade',
			height: '34.52535760728218%',
			minHeight: '300px',
			hover: true,
			pagination: false,
			loaderColor			: '#1f1f1f', 
			loaderBgColor		: 'transparent',
			loaderOpacity		: 1,
			loaderPadding		: 0,
			loaderStroke		: 3,
			});
	});
</script>
<div class="fluid_container">
<div class="camera_container">
<div id="camera_wrap_0">
<div title="slide-1" data-thumb="{{asset('site/components/images/slide-1-1800x546.jpg')}}" data-link="index.php?route=product/product&amp;product_id=43" data-src="images/slide-1-1800x546.jpg">
</div>
<div title="slide-2" data-thumb="{{asset('site/components/images/slide-2-1800x546.jpg')}}" data-link="index.php?route=product/product&amp;product_id=35" data-src="{{asset('site/components/images/slide-2-1800x546.jpg')}}">
</div>
<div title="slide-3" data-thumb="{{asset('site/components/images/slide-3-1800x546.jpg')}}" data-link="index.php?route=product/product&amp;product_id=30" data-src="{{asset('site/components/images/slide-3-1800x546.jpg')}}">
</div>
</div>
</div>
<div class="clear"></div>
</div><div class="container">
<div id="banner0" class="banners row">
<div class="col-sm-3 banner-1">
<div class="banner-box">
<a class="clearfix" href="index.php?route=product/product&amp;product_id=46">
<img src="{{asset('site/components/images/banner-1-270x164.jpg')}}" alt="banner-1" class="img-responsive"/>
<div class="s-desc"><h2>Meat & Fish</h2></div>
</a>
</div>
</div>
<div class="col-sm-3 banner-2">
<div class="banner-box">
<a class="clearfix" href="index.php?route=product/product&amp;product_id=43">
<img src="{{asset('site/components/images/banner-2-270x164.jpg')}}" alt="banner-2" class="img-responsive"/>
<div class="s-desc"><h2>Vegetables</h2></div>
</a>
</div>
</div>
<div class="col-sm-3 banner-3">
<div class="banner-box">
<a class="clearfix" href="index.php?route=product/product&amp;product_id=34">
<img src="{{asset('site/components/images/banner-3-270x164.jpg')}}" alt="banner-3" class="img-responsive"/>
<div class="s-desc"><h2>Beverages</h2></div>
</a>
</div>
</div>
<div class="col-sm-3 banner-4">
<div class="banner-box">
<a class="clearfix" href="index.php?route=product/product&amp;product_id=49">
<img src="{{asset('site/components/images/banner-4-270x164.jpg')}}" alt="banner-4" class="img-responsive"/>
<div class="s-desc"><h2>Fruits</h2></div>
</a>
</div>
</div>
</div>
</div>
<div class="box_html welcome" style="padding:50px 50px;">
<!--<div class="container">
<h2>Welcome</h2>
<p>Beciegast nveriti vitaesaert asety kertya aserde nerafae kertyur styasnemo faserani <br>
ptaiades kertyaser daesraeds. Casrolern atur aut oditatur magni dolratione voluptate msequis keredenasa miaseras <br>
selertas esciun tisquam eiucidut labore et dolore magnam.</p>
<a class="link" href="index.php?route=information/information&information_id=4">read more</a>
</div>--></div>
<script>
    (document).ready(function () {
        (".quickview").fancybox({
            maxWidth: 800,
            maxHeight: 600,
            fitToView: false,
            width: '70%',
            height: '70%',
            autoSize: false,
            closeClick: false,
            openEffect: 'elastic',
            closeEffect: 'elastic',
        });
    });
</script>
<div class="box latest">
<div class="container">
<div class="box-heading"><h3>New Products</h3></div>
<div class="box-content">
<div class="row">
<div class="product-layout col-sm-3 col-lg-3 col-md-3 ">
<div class="product-thumb transition">
<div class="quick_info">
<div id="quickview_latest_1" class="quickview-style">
<div>
<div class="left col-sm-4">
<div class="quickview_image image">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/product&amp;product_id=49"><img alt="Mauris gravida" title="Mauris gravida" class="img-responsive" src="images/product-49-270x205.png"/></a>
</div>
</div>
<div class="right col-sm-8">
<h2>Mauris gravida</h2>
<div class="inf">
<p class="product_model model">Model: SAM1</p>
<div class="price">
<span class="price-new">150.00</span> <span class="price-old">199.99</span>
</div>
</div>
<div class="cart-button">
<button class="btn btn-add" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('49');">
<i class="fa fa-shopping-cart"></i>
</button>
<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('49');">
<i class="fa fa-heart"></i></button>
<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('49');">
<i class="fa fa-exchange"></i></button>
</div>
<div class="clear"></div>
<div class="rating">
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
</div>
</div>
<div class="col-sm-12">
<div class="quickview_description description">
<iframe src="//www.youtube.com/embed/ctAEWKgCSZA?rel=0&amp;controls=0&amp;showinfo=0" allowfullscreen="" frameborder="0" height="315" width="560"></iframe><p><b>Our Food shop</b> was founded in 2001 and only in 2004 we've launched the online version of our store. We are glad to welcome customers from all over the world and offer the best food and ingredients for your joy!</p>
<p><b>Our Food shop</b> was founded in 2001 and only in 2004 we've launched the online version of our store. We are glad to welcome customers from all over the world and offer the best food and ingredients for your joy! We know that it is almost impossible to satisfy all cuisine whims of our visitors but we are working on it – <b>so if you can't find</b> what you were looking for, please <b>feel</b> free to contact our awesome Support to leave your request and we'll get back to you as soon as possible.</p>
<p><b>Flawless quality and low prices</b> – these are the main cornerstones that have helped us build our business. There is nothing more important than our client's needs and perfect reputation of our shop. Sharing is everything and we are glad to share our latest creative ideas about banquet menus and decoration, original recipes etc.</p> </div>
</div>
</div>
</div>
</div>
<div class="new_pr">New</div>
<div class="image">
<a href="#">
<img src="images/product-49-270x205.png" alt="Mauris gravida" title="Mauris gravida" class="img-responsive"/>
</a>
</div>
<div class="caption">
<div class="price">
<span class="price-new">
150.00 </span>
<span class="price-old">
199.99 </span>
</div>
<div class="name">
<a href="#">
Cauliflower </a>
</div>
 
 
 
</div>
<div class="cart-button">
<button class="btn btn-add" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('49');">
<span>Add to Cart</span>
</button>
<a class="quickview quickview-latest btn" data-toggle="tooltip" title="Quick View" rel="group" href="#quickview_latest_1">
Quick View </a>
</div>
<div class="rating">
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
</div>
<div class="clear"></div>
</div>
</div>
<div class="product-layout col-sm-3 col-lg-3 col-md-3 ">
<div class="product-thumb transition">
<div class="quick_info">
<div id="quickview_latest_2" class="quickview-style">
<div>
<div class="left col-sm-4">
<div class="quickview_image image">
<a href="#" title="Praesent imperdiet" class="img-responsive" src="images/product-19-270x205.png"/></a>
</div>
</div>
<div class="right col-sm-8">
<h2>Broccolis</h2>
<div class="inf">
<p class="quickview_manufacture manufacture manufacture">Brand: <a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/manufacturer/info&amp;manufacturer_id=8">Lorem ipsum</a>
</p>
<p class="product_model model">Model: product 20</p>
<div class="price">
100.00 </div>
</div>
<div class="cart-button">
<button class="btn btn-add" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('48');">
<i class="fa fa-shopping-cart"></i>
</button>
<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('48');">
<i class="fa fa-heart"></i></button>
<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('48');">
<i class="fa fa-exchange"></i></button>
</div>
<div class="clear"></div>
<div class="rating">
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
</div>
</div>
<div class="col-sm-12">
<div class="quickview_description description">
<iframe src="//www.youtube.com/embed/ctAEWKgCSZA?rel=0&amp;controls=0&amp;showinfo=0" allowfullscreen="" frameborder="0" height="315" width="560"></iframe><p><b>Our Food shop</b> was founded in 2001 and only in 2004 we've launched the online version of our store. We are glad to welcome customers from all over the world and offer the best food and ingredients for your joy!</p>
<p><b>Our Food shop</b> was founded in 2001 and only in 2004 we've launched the online version of our store. We are glad to welcome customers from all over the world and offer the best food and ingredients for your joy! We know that it is almost impossible to satisfy all cuisine whims of our visitors but we are working on it – <b>so if you can't find</b> what you were looking for, please <b>feel</b> free to contact our awesome Support to leave your request and we'll get back to you as soon as possible.</p>
<p><b>Flawless quality and low prices</b> – these are the main cornerstones that have helped us build our business. There is nothing more important than our client's needs and perfect reputation of our shop. Sharing is everything and we are glad to share our latest creative ideas about banquet menus and decoration, original recipes etc.</p> </div>
</div>
</div>
</div>
</div>
<div class="new_pr">New</div>
<div class="image">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/product&amp;product_id=48">
<img src="images/product-19-270x205.png" alt="Praesent imperdiet" title="Praesent imperdiet" class="img-responsive"/>
</a>
</div>
<div class="caption">
<div class="price">
100.00 </div>
<div class="name">
<a href="#">
Broccolis </a>
</div>
 
 
 
</div>
<div class="cart-button">
<button class="btn btn-add" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('48');">
<span>Add to Cart</span>
</button>
<a class="quickview quickview-latest btn" data-toggle="tooltip" title="Quick View" rel="group" href="#quickview_latest_2">
Quick View </a>
</div>
<div class="rating">
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
</div>
<div class="clear"></div>
</div>
</div>
<div class="product-layout col-sm-3 col-lg-3 col-md-3 ">
<div class="product-thumb transition">
<div class="quick_info">
<div id="quickview_latest_3" class="quickview-style">
<div>
<div class="left col-sm-4">
<div class="quickview_image image">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/product&amp;product_id=47"><img alt="Consectetur adipiscing" title="Consectetur adipiscing" class="img-responsive" src="images/product-12-270x205.png"/></a>
</div>
</div>
<div class="right col-sm-8">
<h2>Consectetur adipiscing</h2>
<div class="inf">
<p class="quickview_manufacture manufacture manufacture">Brand: <a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/manufacturer/info&amp;manufacturer_id=7">libero convallis</a>
</p>
<p class="product_model model">Model: Product 21</p>
<div class="price">
100.00 </div>
</div>
<div class="cart-button">
<button class="btn btn-add" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('47');">
<i class="fa fa-shopping-cart"></i>
</button>
<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('47');">
<i class="fa fa-heart"></i></button>
<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('47');">
<i class="fa fa-exchange"></i></button>
</div>
<div class="clear"></div>
<div class="rating">
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
</div>
</div>
<div class="col-sm-12">
<div class="quickview_description description">
<iframe src="//www.youtube.com/embed/ctAEWKgCSZA?rel=0&amp;controls=0&amp;showinfo=0" allowfullscreen="" frameborder="0" height="315" width="560"></iframe><p><b>Our Food shop</b> was founded in 2001 and only in 2004 we've launched the online version of our store. We are glad to welcome customers from all over the world and offer the best food and ingredients for your joy!</p>
<p><b>Our Food shop</b> was founded in 2001 and only in 2004 we've launched the online version of our store. We are glad to welcome customers from all over the world and offer the best food and ingredients for your joy! We know that it is almost impossible to satisfy all cuisine whims of our visitors but we are working on it – <b>so if you can't find</b> what you were looking for, please <b>feel</b> free to contact our awesome Support to leave your request and we'll get back to you as soon as possible.</p>
<p><b>Flawless quality and low prices</b> – these are the main cornerstones that have helped us build our business. There is nothing more important than our client's needs and perfect reputation of our shop. Sharing is everything and we are glad to share our latest creative ideas about banquet menus and decoration, original recipes etc.</p> </div>
</div>
</div>
</div>
</div>
<div class="new_pr">New</div>
<div class="image">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/product&amp;product_id=47">
<img src="images/product-12-270x205.png" alt="Consectetur adipiscing" title="Consectetur adipiscing" class="img-responsive"/>
</a>
</div>
<div class="caption">
<div class="price">
100.00 </div>
<div class="name">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/product&amp;product_id=47">
Consectetur adipiscing </a>
</div>
 
 
 
</div>
<div class="cart-button">
<button class="btn btn-add" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('47');">
<span>Add to Cart</span>
</button>
<a class="quickview quickview-latest btn" data-toggle="tooltip" title="Quick View" rel="group" href="#quickview_latest_3">
Quick View </a>
</div>
<div class="rating">
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
</div>
<div class="clear"></div>
</div>
</div>
<div class="product-layout col-sm-3 col-lg-3 col-md-3 ">
<div class="product-thumb transition">
<div class="quick_info">
<div id="quickview_latest_4" class="quickview-style">
<div>
<div class="left col-sm-4">
<div class="quickview_image image">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/product&amp;product_id=46"><img alt="Suspendisse imperdiet" title="Suspendisse imperdiet" class="img-responsive" src="{{asset('site/components/images/product-55-270x205.png')}}"/></a>
</div>
</div>
<div class="right col-sm-8">
<h2>Suspendisse imperdiet</h2>
<div class="inf">
<p class="quickview_manufacture manufacture manufacture">Brand: <a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/manufacturer/info&amp;manufacturer_id=10">Fusce vestibulum</a>
</p>
<p class="product_model model">Model: Product 19</p>
<div class="price">
1,000.00 </div>
</div>
<div class="cart-button">
<button class="btn btn-add" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('46');">
<i class="fa fa-shopping-cart"></i>
</button>
<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('46');">
<i class="fa fa-heart"></i></button>
<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('46');">
<i class="fa fa-exchange"></i></button>
</div>
<div class="clear"></div>
<div class="rating">
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
</div>
</div>
<div class="col-sm-12">
<div class="quickview_description description">
<iframe src="//www.youtube.com/embed/ctAEWKgCSZA?rel=0&amp;controls=0&amp;showinfo=0" allowfullscreen="" frameborder="0" height="315" width="560"></iframe><p><b>Our Food shop</b> was founded in 2001 and only in 2004 we've launched the online version of our store. We are glad to welcome customers from all over the world and offer the best food and ingredients for your joy!</p>
<p><b>Our Food shop</b> was founded in 2001 and only in 2004 we've launched the online version of our store. We are glad to welcome customers from all over the world and offer the best food and ingredients for your joy! We know that it is almost impossible to satisfy all cuisine whims of our visitors but we are working on it – <b>so if you can't find</b> what you were looking for, please <b>feel</b> free to contact our awesome Support to leave your request and we'll get back to you as soon as possible.</p>
<p><b>Flawless quality and low prices</b> – these are the main cornerstones that have helped us build our business. There is nothing more important than our client's needs and perfect reputation of our shop. Sharing is everything and we are glad to share our latest creative ideas about banquet menus and decoration, original recipes etc.</p> </div>
</div>
</div>
</div>
</div>
<div class="new_pr">New</div>
<div class="image">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/product&amp;product_id=46">
<img src="{{asset('site/components/images/product-55-270x205.png')}}" alt="Suspendisse imperdiet" title="Suspendisse imperdiet" class="img-responsive"/>
</a>
</div>
<div class="caption">
<div class="price">
1,000.00 </div>
<div class="name">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/product&amp;product_id=46">
Suspendisse imperdiet </a>
</div>
 
 
 
</div>
<div class="cart-button">
<button class="btn btn-add" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('46');">
<span>Add to Cart</span>
</button>
<a class="quickview quickview-latest btn" data-toggle="tooltip" title="Quick View" rel="group" href="#quickview_latest_4">
Quick View </a>
</div>
<div class="rating">
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
</div>
<div class="clear"></div>
</div>
</div>
<div class="product-layout col-sm-3 col-lg-3 col-md-3 last">
<div class="product-thumb transition">
<div class="quick_info">
<div id="quickview_latest_5" class="quickview-style">
<div>
<div class="left col-sm-4">
<div class="quickview_image image">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/product&amp;product_id=45"><img alt="Phasellus vel scelerisque" title="Phasellus vel scelerisque" class="img-responsive" src="images/product-37-270x205.png"/></a>
</div>
</div>
<div class="right col-sm-8">
<h2>Phasellus vel scelerisque</h2>
<div class="inf">
<p class="quickview_manufacture manufacture manufacture">Brand: <a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/manufacturer/info&amp;manufacturer_id=8">Lorem ipsum</a>
</p>
<p class="product_model model">Model: Product 18</p>
<div class="price">
<span class="price-new">150.00</span> <span class="price-old">200.00</span>
</div>
</div>
<div class="cart-button">
<button class="btn btn-add" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('45');">
<i class="fa fa-shopping-cart"></i>
</button>
<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('45');">
<i class="fa fa-heart"></i></button>
<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('45');">
<i class="fa fa-exchange"></i></button>
</div>
<div class="clear"></div>
<div class="rating">
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
</div>
</div>
<div class="col-sm-12">
<div class="quickview_description description">
<iframe src="//www.youtube.com/embed/ctAEWKgCSZA?rel=0&amp;controls=0&amp;showinfo=0" allowfullscreen="" frameborder="0" height="315" width="560"></iframe><p><b>Our Food shop</b> was founded in 2001 and only in 2004 we've launched the online version of our store. We are glad to welcome customers from all over the world and offer the best food and ingredients for your joy!</p>
<p><b>Our Food shop</b> was founded in 2001 and only in 2004 we've launched the online version of our store. We are glad to welcome customers from all over the world and offer the best food and ingredients for your joy! We know that it is almost impossible to satisfy all cuisine whims of our visitors but we are working on it – <b>so if you can't find</b> what you were looking for, please <b>feel</b> free to contact our awesome Support to leave your request and we'll get back to you as soon as possible.</p>
<p><b>Flawless quality and low prices</b> – these are the main cornerstones that have helped us build our business. There is nothing more important than our client's needs and perfect reputation of our shop. Sharing is everything and we are glad to share our latest creative ideas about banquet menus and decoration, original recipes etc.</p> </div>
</div>
</div>
</div>
</div>
<div class="new_pr">New</div>
<div class="image">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/product&amp;product_id=45">
<img src="images/product-37-270x205.png" alt="Phasellus vel scelerisque" title="Phasellus vel scelerisque" class="img-responsive"/>
</a>
</div>
<div class="caption">
<div class="price">
<span class="price-new">
150.00 </span>
<span class="price-old">
200.00 </span>
</div>
<div class="name">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/product&amp;product_id=45">
Phasellus vel scelerisque </a>
</div>
 
 
 
</div>
<div class="cart-button">
<button class="btn btn-add" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('45');">
<span>Add to Cart</span>
</button>
<a class="quickview quickview-latest btn" data-toggle="tooltip" title="Quick View" rel="group" href="#quickview_latest_5">
Quick View </a>
</div>
<div class="rating">
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
</div>
<div class="clear"></div>
</div>
</div>
<div class="product-layout col-sm-3 col-lg-3 col-md-3 ">
<div class="product-thumb transition">
<div class="quick_info">
<div id="quickview_latest_6" class="quickview-style">
<div>
<div class="left col-sm-4">
<div class="quickview_image image">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/product&amp;product_id=44"><img alt="Donec non posuere" title="Donec non posuere" class="img-responsive" src="{{asset('site/components/images/product-34-270x205.png')}}"/></a>
</div>
</div>
<div class="right col-sm-8">
<h2>Donec non posuere</h2>
<div class="inf">
<p class="quickview_manufacture manufacture manufacture">Brand: <a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/manufacturer/info&amp;manufacturer_id=8">Lorem ipsum</a>
</p>
<p class="product_model model">Model: Product 17</p>
<div class="price">
<span class="price-new">90.00</span> <span class="price-old">1,000.00</span>
</div>
</div>
<div class="cart-button">
<button class="btn btn-add" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('44');">
<i class="fa fa-shopping-cart"></i>
</button>
<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('44');">
<i class="fa fa-heart"></i></button>
<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('44');">
<i class="fa fa-exchange"></i></button>
</div>
<div class="clear"></div>
<div class="rating">
<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
</div>
</div>
<div class="col-sm-12">
<div class="quickview_description description">
<iframe src="//www.youtube.com/embed/ctAEWKgCSZA?rel=0&amp;controls=0&amp;showinfo=0" allowfullscreen="" frameborder="0" height="315" width="560"></iframe><p><b>Our Food shop</b> was founded in 2001 and only in 2004 we've launched the online version of our store. We are glad to welcome customers from all over the world and offer the best food and ingredients for your joy!</p>
<p><b>Our Food shop</b> was founded in 2001 and only in 2004 we've launched the online version of our store. We are glad to welcome customers from all over the world and offer the best food and ingredients for your joy! We know that it is almost impossible to satisfy all cuisine whims of our visitors but we are working on it – <b>so if you can't find</b> what you were looking for, please <b>feel</b> free to contact our awesome Support to leave your request and we'll get back to you as soon as possible.</p>
<p><b>Flawless quality and low prices</b> – these are the main cornerstones that have helped us build our business. There is nothing more important than our client's needs and perfect reputation of our shop. Sharing is everything and we are glad to share our latest creative ideas about banquet menus and decoration, original recipes etc.</p> </div>
</div>
</div>
</div>
</div>
<div class="new_pr">New</div>
<div class="image">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/product&amp;product_id=44">
<img src="images/product-34-270x205.png" alt="Donec non posuere" title="Donec non posuere" class="img-responsive"/>
</a>
</div>
<div class="caption">
<div class="price">
<span class="price-new">
90.00 </span>
<span class="price-old">
1,000.00 </span>
</div>
<div class="name">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/product&amp;product_id=44">
Donec non posuere </a>
</div>
 
 
 
</div>
<div class="cart-button">
<button class="btn btn-add" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('44');">
<span>Add to Cart</span>
</button>
<a class="quickview quickview-latest btn" data-toggle="tooltip" title="Quick View" rel="group" href="#quickview_latest_6">
Quick View </a>
</div>
<div class="clear"></div>
</div>
</div>
<div class="product-layout col-sm-3 col-lg-3 col-md-3 ">
<div class="product-thumb transition">
<div class="quick_info">
<div id="quickview_latest_7" class="quickview-style">
<div>
<div class="left col-sm-4">
<div class="quickview_image image">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/product&amp;product_id=43"><img alt="Quisque eget" title="Quisque eget" class="img-responsive" src="images/product-31-270x205.png"/></a>
</div>
</div>
<div class="right col-sm-8">
<h2>Quisque eget</h2>
<div class="inf">
<p class="quickview_manufacture manufacture manufacture">Brand: <a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/manufacturer/info&amp;manufacturer_id=8">Lorem ipsum</a>
</p>
<p class="product_model model">Model: Product 16</p>
<div class="price">
<span class="price-new">400.00</span> <span class="price-old">500.00</span>
</div>
</div>
<div class="cart-button">
<button class="btn btn-add" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('43');">
<i class="fa fa-shopping-cart"></i>
</button>
<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('43');">
<i class="fa fa-heart"></i></button>
<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('43');">
<i class="fa fa-exchange"></i></button>
</div>
<div class="clear"></div>
<div class="rating">
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
</div>
</div>
<div class="col-sm-12">
<div class="quickview_description description">
<iframe src="//www.youtube.com/embed/ctAEWKgCSZA?rel=0&amp;controls=0&amp;showinfo=0" allowfullscreen="" frameborder="0" height="315" width="560"></iframe><p><b>Our Food shop</b> was founded in 2001 and only in 2004 we've launched the online version of our store. We are glad to welcome customers from all over the world and offer the best food and ingredients for your joy!</p>
<p><b>Our Food shop</b> was founded in 2001 and only in 2004 we've launched the online version of our store. We are glad to welcome customers from all over the world and offer the best food and ingredients for your joy! We know that it is almost impossible to satisfy all cuisine whims of our visitors but we are working on it – <b>so if you can't find</b> what you were looking for, please <b>feel</b> free to contact our awesome Support to leave your request and we'll get back to you as soon as possible.</p>
<p><b>Flawless quality and low prices</b> – these are the main cornerstones that have helped us build our business. There is nothing more important than our client's needs and perfect reputation of our shop. Sharing is everything and we are glad to share our latest creative ideas about banquet menus and decoration, original recipes etc.</p> </div>
</div>
</div>
</div>
</div>
<div class="new_pr">New</div>
<div class="image">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/product&amp;product_id=43">
<img src="images/product-31-270x205.png" alt="Quisque eget" title="Quisque eget" class="img-responsive"/>
</a>
</div>
<div class="caption">
<div class="price">
<span class="price-new">
400.00 </span>
<span class="price-old">
500.00 </span>
</div>
<div class="name">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/product&amp;product_id=43">
Quisque eget </a>
</div>
 
 
 
</div>
<div class="cart-button">
<button class="btn btn-add" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('43');">
<span>Add to Cart</span>
</button>
<a class="quickview quickview-latest btn" data-toggle="tooltip" title="Quick View" rel="group" href="#quickview_latest_7">
Quick View </a>
</div>
<div class="rating">
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
</div>
<div class="clear"></div>
</div>
</div>
<div class="product-layout col-sm-3 col-lg-3 col-md-3 ">
<div class="product-thumb transition">
<div class="quick_info">
<div id="quickview_latest_8" class="quickview-style">
<div>
<div class="left col-sm-4">
<div class="quickview_image image">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/product&amp;product_id=42"><img alt="Lorem ipsum" title="Lorem ipsum" class="img-responsive" src="images/product-22-270x205.png"/></a>
</div>
</div>
<div class="right col-sm-8">
<h2>Lorem ipsum</h2>
<div class="inf">
<p class="quickview_manufacture manufacture manufacture">Brand: <a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/manufacturer/info&amp;manufacturer_id=8">Lorem ipsum</a>
</p>
<p class="product_model model">Model: Product 15</p>
<div class="price">
<span class="price-new">90.00</span> <span class="price-old">100.00</span>
</div>
</div>
<div class="cart-button">
<button class="btn btn-add" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('42');">
<i class="fa fa-shopping-cart"></i>
</button>
<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('42');">
<i class="fa fa-heart"></i></button>
<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('42');">
<i class="fa fa-exchange"></i></button>
</div>
<div class="clear"></div>
<div class="rating">
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
</div>
</div>
<div class="col-sm-12">
<div class="quickview_description description">
<iframe src="//www.youtube.com/embed/ctAEWKgCSZA?rel=0&amp;controls=0&amp;showinfo=0" allowfullscreen="" frameborder="0" height="315" width="560"></iframe><p><b>Our Food shop</b> was founded in 2001 and only in 2004 we've launched the online version of our store. We are glad to welcome customers from all over the world and offer the best food and ingredients for your joy!</p>
<p><b>Our Food shop</b> was founded in 2001 and only in 2004 we've launched the online version of our store. We are glad to welcome customers from all over the world and offer the best food and ingredients for your joy! We know that it is almost impossible to satisfy all cuisine whims of our visitors but we are working on it – <b>so if you can't find</b> what you were looking for, please <b>feel</b> free to contact our awesome Support to leave your request and we'll get back to you as soon as possible.</p>
<p><b>Flawless quality and low prices</b> – these are the main cornerstones that have helped us build our business. There is nothing more important than our client's needs and perfect reputation of our shop. Sharing is everything and we are glad to share our latest creative ideas about banquet menus and decoration, original recipes etc.</p> </div>
</div>
</div>
</div>
</div>
<div class="new_pr">New</div>
<div class="image">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/product&amp;product_id=42">
<img src="images/product-22-270x205.png" alt="Lorem ipsum" title="Lorem ipsum" class="img-responsive"/>
</a>
</div>
<div class="caption">
<div class="price">
<span class="price-new">
90.00 </span>
<span class="price-old">
100.00 </span>
</div>
<div class="name">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/product&amp;product_id=42">
Lorem ipsum </a>
</div>
 
 
 
</div>
<div class="cart-button">
<button class="btn btn-add" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('42');">
<span>Add to Cart</span>
</button>
<a class="quickview quickview-latest btn" data-toggle="tooltip" title="Quick View" rel="group" href="#quickview_latest_8">
Quick View </a>
</div>
<div class="rating">
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
</div>
<div class="clear"></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="box_html video_block">
<!--<div class="vide " data-vide-bg="images/catalog/video/video_1">
<div class="container">
<h2>Fresh!</h2>
<h3>100% organic <a href="index.php?route=product/product&product_id=29" class="fa fa-chevron-right"></a></h3>
<p>Asetkertya aserde nerase yasnemo fasnis ptaiades kertyaser daeses rolern atur aut oditatus.</p>
</div>
</div>--></div>
<script>
    (document).ready(function () {
        (".quickview").fancybox({
            maxWidth: 800,
            maxHeight: 600,
            fitToView: false,
            width: '70%',
            height: '70%',
            autoSize: false,
            closeClick: false,
            openEffect: 'elastic',
            closeEffect: 'elastic',
        });
    });
</script>
<div class="box specials">
<div class="container">
<div class="box-heading"><h3>Specials</h3></div>
<div class="box-content">
<div class="row">
<div class="product-layout col-sm-3 col-md-3 col-lg-3">
<div class="product-thumb transition">
<div class="quick_info">
<div id="quickview_special_1" class="quickview-style">
<div>
<div class="left col-sm-4">
<div class="quickview_image image">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/product&amp;product_id=33"><img alt="Aliquam dolor tellus" title="Aliquam dolor tellus" class="img-responsive" src="images/product-52-270x205.png"/></a>
</div>
</div>
<div class="right col-sm-8">
<h2>Aliquam dolor tellus</h2>
<div class="inf">
<p class="quickview_manufacture manufacture manufacture">Brand: <a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/manufacturer/info&amp;manufacturer_id=10">Fusce vestibulum</a>
</p>
<p class="product_model model">Model: Product 6</p>
<div class="price">
<span class="price-new">150.00</span> <span class="price-old">200.00</span>
</div>
</div>
<div class="">
<button class="btn btn-add" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('33');">
<i class="fa fa-shopping-cart"></i>
</button>
<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('33');"><i class="fa fa-heart"></i></button>
<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('33');"><i class="fa fa-exchange"></i></button>
</div>
<div class="clear"></div>
<div class="rating">
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
</div>
</div>
<div class="col-sm-12">
<div class="quickview_description description">
<iframe src="//www.youtube.com/embed/ctAEWKgCSZA?rel=0&amp;controls=0&amp;showinfo=0" allowfullscreen="" frameborder="0" height="315" width="560"></iframe><p><b>Our Food shop</b> was founded in 2001 and only in 2004 we've launched the online version of our store. We are glad to welcome customers from all over the world and offer the best food and ingredients for your joy!</p>
<p><b>Our Food shop</b> was founded in 2001 and only in 2004 we've launched the online version of our store. We are glad to welcome customers from all over the world and offer the best food and ingredients for your joy! We know that it is almost impossible to satisfy all cuisine whims of our visitors but we are working on it – <b>so if you can't find</b> what you were looking for, please <b>feel</b> free to contact our awesome Support to leave your request and we'll get back to you as soon as possible.</p>
<p><b>Flawless quality and low prices</b> – these are the main cornerstones that have helped us build our business. There is nothing more important than our client's needs and perfect reputation of our shop. Sharing is everything and we are glad to share our latest creative ideas about banquet menus and decoration, original recipes etc.</p> </div>
</div>
</div>
</div>
</div>
<div class="sale">Sale</div>
<div class="image">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/product&amp;product_id=33">
<img src="images/product-52-270x205.png" alt="Aliquam dolor tellus" title="Aliquam dolor tellus" class="img-responsive"/>
</a>
</div>
<div class="caption">
<div class="price">
<span class="price-new">
150.00 </span>
<span class="price-old">
200.00 </span>
</div>
<div class="name">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/product&amp;product_id=33">
Aliquam dolor tellus </a>
</div>
 
 
 
<div class="rating">
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
</div>
</div>
<div class="cart-button">
<button class="btn btn-add" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('33');">
<span>Add to Cart</span>
</button>
<a class="quickview quickview-latest btn" data-toggle="tooltip" title="Quick View" rel="group" href="#quickview_special_1">
Quick View </a>
</div>
<div class="clear"></div>
</div>
</div>
<div class="product-layout col-sm-3 col-md-3 col-lg-3">
<div class="product-thumb transition">
<div class="quick_info">
<div id="quickview_special_2" class="quickview-style">
<div>
<div class="left col-sm-4">
<div class="quickview_image image">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/product&amp;product_id=28"><img alt="Aliquam eget" title="Aliquam eget" class="img-responsive" src="images/product-10-270x205.png"/></a>
</div>
</div>
<div class="right col-sm-8">
<h2>Aliquam eget</h2>
<div class="inf">
<p class="quickview_manufacture manufacture manufacture">Brand: <a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/manufacturer/info&amp;manufacturer_id=5">Donec eu</a>
</p>
<p class="product_model model">Model: Product 1</p>
<div class="price">
<span class="price-new">80.00</span> <span class="price-old">100.00</span>
</div>
</div>
<div class="">
<button class="btn btn-add" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('28');">
<i class="fa fa-shopping-cart"></i>
</button>
<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('28');"><i class="fa fa-heart"></i></button>
<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('28');"><i class="fa fa-exchange"></i></button>
</div>
<div class="clear"></div>
<div class="rating">
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
</div>
</div>
<div class="col-sm-12">
<div class="quickview_description description">
<iframe src="//www.youtube.com/embed/ctAEWKgCSZA?rel=0&amp;controls=0&amp;showinfo=0" allowfullscreen="" frameborder="0" height="315" width="560"></iframe><p><b>Our Food shop</b> was founded in 2001 and only in 2004 we've launched the online version of our store. We are glad to welcome customers from all over the world and offer the best food and ingredients for your joy!</p>
<p><b>Our Food shop</b> was founded in 2001 and only in 2004 we've launched the online version of our store. We are glad to welcome customers from all over the world and offer the best food and ingredients for your joy! We know that it is almost impossible to satisfy all cuisine whims of our visitors but we are working on it – <b>so if you can't find</b> what you were looking for, please <b>feel</b> free to contact our awesome Support to leave your request and we'll get back to you as soon as possible.</p>
<p><b>Flawless quality and low prices</b> – these are the main cornerstones that have helped us build our business. There is nothing more important than our client's needs and perfect reputation of our shop. Sharing is everything and we are glad to share our latest creative ideas about banquet menus and decoration, original recipes etc.</p> </div>
</div>
</div>
</div>
</div>
<div class="sale">Sale</div>
<div class="image">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/product&amp;product_id=28">
<img src="images/product-10-270x205.png" alt="Aliquam eget" title="Aliquam eget" class="img-responsive"/>
</a>
</div>
<div class="caption">
<div class="price">
<span class="price-new">
80.00 </span>
<span class="price-old">
100.00 </span>
</div>
<div class="name">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/product&amp;product_id=28">
Aliquam eget </a>
</div>
 
 
 
<div class="rating">
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
</div>
</div>
<div class="cart-button">
<button class="btn btn-add" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('28');">
<span>Add to Cart</span>
</button>
<a class="quickview quickview-latest btn" data-toggle="tooltip" title="Quick View" rel="group" href="#quickview_special_2">
Quick View </a>
</div>
<div class="clear"></div>
</div>
</div>
<div class="product-layout col-sm-3 col-md-3 col-lg-3">
<div class="product-thumb transition">
<div class="quick_info">
<div id="quickview_special_3" class="quickview-style">
<div>
<div class="left col-sm-4">
<div class="quickview_image image">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/product&amp;product_id=30"><img alt="Dolor sit amet" title="Dolor sit amet" class="img-responsive" src="images/product-13-270x205.png"/></a>
</div>
</div>
<div class="right col-sm-8">
<h2>Dolor sit amet</h2>
<div class="inf">
<p class="quickview_manufacture manufacture manufacture">Brand: <a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/manufacturer/info&amp;manufacturer_id=9">Quisque sodales</a>
</p>
<p class="product_model model">Model: Product 3</p>
<div class="price">
<span class="price-new">80.00</span> <span class="price-old">100.00</span>
</div>
</div>
<div class="">
<button class="btn btn-add" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('30');">
<i class="fa fa-shopping-cart"></i>
</button>
<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('30');"><i class="fa fa-heart"></i></button>
<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('30');"><i class="fa fa-exchange"></i></button>
</div>
<div class="clear"></div>
<div class="rating">
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
</div>
</div>
<div class="col-sm-12">
<div class="quickview_description description">
<iframe src="//www.youtube.com/embed/ctAEWKgCSZA?rel=0&amp;controls=0&amp;showinfo=0" allowfullscreen="" frameborder="0" height="315" width="560"></iframe><p><b>Our Food shop</b> was founded in 2001 and only in 2004 we've launched the online version of our store. We are glad to welcome customers from all over the world and offer the best food and ingredients for your joy!</p>
<p><b>Our Food shop</b> was founded in 2001 and only in 2004 we've launched the online version of our store. We are glad to welcome customers from all over the world and offer the best food and ingredients for your joy! We know that it is almost impossible to satisfy all cuisine whims of our visitors but we are working on it – <b>so if you can't find</b> what you were looking for, please <b>feel</b> free to contact our awesome Support to leave your request and we'll get back to you as soon as possible.</p>
<p><b>Flawless quality and low prices</b> – these are the main cornerstones that have helped us build our business. There is nothing more important than our client's needs and perfect reputation of our shop. Sharing is everything and we are glad to share our latest creative ideas about banquet menus and decoration, original recipes etc.</p> </div>
</div>
</div>
</div>
</div>
<div class="sale">Sale</div>
<div class="image">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/product&amp;product_id=30">
<img src="images/product-13-270x205.png" alt="Dolor sit amet" title="Dolor sit amet" class="img-responsive"/>
</a>
</div>
<div class="caption">
<div class="price">
<span class="price-new">
80.00 </span>
<span class="price-old">
100.00 </span>
</div>
<div class="name">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/product&amp;product_id=30">
Dolor sit amet </a>
</div>
 
 
 
<div class="rating">
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
<span class="fa fa-stack">
<i class="fa fa-star fa-stack-2x"></i>
<i class="fa fa-star-o fa-stack-2x"></i>
</span>
</div>
</div>
<div class="cart-button">
<button class="btn btn-add" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('30');">
<span>Add to Cart</span>
</button>
<a class="quickview quickview-latest btn" data-toggle="tooltip" title="Quick View" rel="group" href="#quickview_special_3">
Quick View </a>
</div>
<div class="clear"></div>
</div>
</div>
<div class="product-layout col-sm-3 col-md-3 col-lg-3">
<div class="product-thumb transition">
<div class="quick_info">
<div id="quickview_special_4" class="quickview-style">
<div>
<div class="left col-sm-4">
<div class="quickview_image image">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/product&amp;product_id=44"><img alt="Donec non posuere" title="Donec non posuere" class="img-responsive" src="images/product-34-270x205.png"/></a>
</div>
</div>
<div class="right col-sm-8">
<h2>Donec non posuere</h2>
<div class="inf">
<p class="quickview_manufacture manufacture manufacture">Brand: <a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/manufacturer/info&amp;manufacturer_id=8">Lorem ipsum</a>
</p>
<p class="product_model model">Model: Product 17</p>
<div class="price">
<span class="price-new">90.00</span> <span class="price-old">1,000.00</span>
</div>
</div>
<div class="">
<button class="btn btn-add" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('44');">
<i class="fa fa-shopping-cart"></i>
</button>
<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Add to Wish List" onclick="wishlist.add('44');"><i class="fa fa-heart"></i></button>
<button class="btn btn-icon" type="button" data-toggle="tooltip" title="Compare this Product" onclick="compare.add('44');"><i class="fa fa-exchange"></i></button>
</div>
<div class="clear"></div>
<div class="rating">
<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
</div>
</div>
<div class="col-sm-12">
<div class="quickview_description description">
<iframe src="//www.youtube.com/embed/ctAEWKgCSZA?rel=0&amp;controls=0&amp;showinfo=0" allowfullscreen="" frameborder="0" height="315" width="560"></iframe><p><b>Our Food shop</b> was founded in 2001 and only in 2004 we've launched the online version of our store. We are glad to welcome customers from all over the world and offer the best food and ingredients for your joy!</p>
<p><b>Our Food shop</b> was founded in 2001 and only in 2004 we've launched the online version of our store. We are glad to welcome customers from all over the world and offer the best food and ingredients for your joy! We know that it is almost impossible to satisfy all cuisine whims of our visitors but we are working on it – <b>so if you can't find</b> what you were looking for, please <b>feel</b> free to contact our awesome Support to leave your request and we'll get back to you as soon as possible.</p>
<p><b>Flawless quality and low prices</b> – these are the main cornerstones that have helped us build our business. There is nothing more important than our client's needs and perfect reputation of our shop. Sharing is everything and we are glad to share our latest creative ideas about banquet menus and decoration, original recipes etc.</p> </div>
</div>
</div>
</div>
</div>
<div class="sale">Sale</div>
<div class="image">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/product&amp;product_id=44">
<img src="images/product-34-270x205.png" alt="Donec non posuere" title="Donec non posuere" class="img-responsive"/>
</a>
</div>
<div class="caption">
<div class="price">
<span class="price-new">
90.00 </span>
<span class="price-old">
1,000.00 </span>
</div>
<div class="name">
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/product&amp;product_id=44">
Donec non posuere </a>
</div>
 
 
 
</div>
<div class="cart-button">
<button class="btn btn-add" type="button" data-toggle="tooltip" title="Add to Cart" onclick="cart.add('44');">
<span>Add to Cart</span>
</button>
<a class="quickview quickview-latest btn" data-toggle="tooltip" title="Quick View" rel="group" href="#quickview_special_4">
Quick View </a>
</div>
<div class="clear"></div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="box_html social">
<div class="container"> <ul class="social-list row">
<li class="col-sm-3 col-xs-6"> <a class="fa fa-twitter" href="#"></a> </li>
<li class="col-sm-3 col-xs-6"> <a class="fa fa-google-plus" href="#"></a> </li>
<li class="col-sm-3 col-xs-6"> <a class="fa fa-rss" href="#"></a> </li>
<li class="col-sm-3 col-xs-6"> <a class="fa fa-linkedin" href="#"></a> </li>
</ul>
</div></div>
</div>
<div id="container">
<div class="container">
<div class="row"><!--<aside id="column-left" class="col-sm-3 ">
<div class="box facebook info">
<div class="box-heading">
<h3>Facebook</h3>
</div>
<div class="box-content">
<div class="fb-like-box" data-href="https://www.facebook.com/TemplateMonster" data-width="270" data-height="250" data-show-faces="1" data-stream="0" data-header="0" data-colorscheme="light" data-show-border="0"></div>
</div>
</div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
            xfbml      : true,
      version    : 'v2.2'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
</aside>-->
<!--<div id="content" class="col-sm-9"><div class="box_html gmap">
<div class="map">
<div id="google-map" class="map_model"></div>
<ul class="map_locations">
<li data-x="-73.9874068" data-y="40.643180">
<p> 9870 St Vincent Place, Glasgow, DC 45 Fr 45. <span>800 2345-6789</span></p>
</li>
</ul>
</div></div>
</div>-->
</div>
</div>
</div>
<div class="content_bottom">
</div>
<footer>
@nclude('site.includes.footer')

</footer>
<script src="{{asset('site/components/js/livesearch.js')}}" type="text/javascript"></script>
<script src="{{asset('site/components/js/script.js')}}" type="text/javascript"></script>
</div>
 
<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
f[z]=function(){
(a.s=a.s||[]).push(arguments)};var a=f[z]._={
},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
0:+new Date};a.P=function(u){
a.p[u]=new Date-a.p[0]};function s(){
a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
b.contentWindow[g].open()}catch(w){
c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
/* custom configuration goes here (www.olark.com/documentation) */
olark.identify('7830-582-10-3714');/*]]>*/</script><noscript><a href="https://www.olark.com/site/7830-582-10-3714/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>
 
<script type="text/javascript">/* CloudFlare analytics upgrade */
</script>
<script type="text/javascript">if(!NREUMQ.f){NREUMQ.f=function(){NREUMQ.push(["load",new Date().getTime()]);var e=document.createElement("script");e.type="text/javascript";e.src=(("http:"===document.location.protocol)?"http:":"https:")+"//"+"js-agent.newrelic.com/nr-100.js";document.body.appendChild(e);if(NREUMQ.a)NREUMQ.a();};NREUMQ.a=window.onload;window.onload=NREUMQ.f;};NREUMQ.push(["nrfj","beacon-1.newrelic.com","72d7dcce33","1388850","ZV1TZ0FTVkFVWkwKXlwXZEFaHRIdXVdcBkkcSFlD",0,121,new Date().getTime(),"","","","",""]);</script></body></html>
