<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ecom_currency extends Model
{
	protected $table="ecom_currencies";

	protected $fillable = ['currency_name', 'currency_symbol', 'is_active'];
    //
}
