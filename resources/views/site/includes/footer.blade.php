<div class="container">
<div class="row">
<div class="col-lg-3 col-md-3 col-sm-6">
<div class="footer_box">
<h5 data-equal-group="5">Extras</h5>
<ul class="list-unstyled">
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/manufacturer">Brands</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=account/voucher">Gift Vouchers</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=affiliate/account">Affiliates</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=product/special">Specials</a>
</li>
</ul>
</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6">
<div class="footer_box">
<h5 data-equal-group="5">Information</h5>
<ul class="list-unstyled">
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=information/information&amp;information_id=4">About Us</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=information/information&amp;information_id=6">Delivery Information</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=information/information&amp;information_id=3">Privacy Policy</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=information/information&amp;information_id=5">Terms &amp; Conditions</a>
</li>
</ul>
</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6">
<div class="footer_box">
<h5 data-equal-group="5">My Account</h5>
<ul class="list-unstyled">
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=account/account">My Account</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=account/order">Order History</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=account/wishlist">Wish List</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=account/newsletter">Newsletter</a>
</li>
</ul>
</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6">
<div class="footer_box">
<h5 data-equal-group="5">Customer Service</h5>
<ul class="list-unstyled">
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=information/contact">Contact Us</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=account/return/add">Returns</a>
</li>
<li>
<a href="http://livedemo00.template-help.com/opencart_53122/index.php?route=information/sitemap">Site Map</a>
</li>
</ul>
</div>
</div>
</div>
</div>
<div class="copyright">
<div class="container">
Powered By <a href="http://amsitsoft.com/">AMS IT </a><br/> &copy; <?php echo date('Y')?> 
</div>
</div>