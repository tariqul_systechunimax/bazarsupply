<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ecom_products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_sku',255);
            $table->string('product_qrcode',255);
            $table->string('product_name',255);
            $table->string('product_size',50);
            $table->text('product_description');
            $table->integer('product_currency_id')->nullable;
            $table->integer('product_unit_id')->nullable;
            $table->string('product_logo_path',255);
            $table->string('product_small_image',255);
            $table->string('product_large_image',255);
            $table->decimal('product_unit_price',5,2)->nullable;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ecom_products');
    }
}
