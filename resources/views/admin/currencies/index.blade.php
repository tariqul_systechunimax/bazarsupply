@extends('admin.layouts.default')

@section('header')
   
@stop


@section('content')

<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">Default Box Example</h3>
    <div class="box-tools pull-right">
      <!-- Buttons, labels, and many other things can be placed here! -->
      <!-- Here is a label for example -->
        <a href="">
        <button class="btn btn-block btn-success btn-flat" type="button">Go Back To List</button>
    </div><!-- /.box-tools -->
  </div><!-- /.box-header -->
  <div class="box-body">

       <div class="alert alert-success">
		  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		  <strong>Success!</strong> Indicates a successful or positive action.
	   </div>
     <div class="clearfix"></div>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
           <strong>Danger!</strong> Indicates a dangerous or potentially negative action.
        </div>
   <div class="clearfix"></div>
     <div class="row">
     	<div class="col-md-6">
     		 <div class="form-group">
                 <label>Currency Name</label>
                   <input type="hidden" id="token" value="{{ csrf_token() }}">
                   <input class="form-control" type="text" id="currency_name" placeholder="Enter ..." />
             </div>
     	</div>
        <div class="col-md-6">
        	 <div class="form-group">
                 <label>Currency Symbol</label>
                   <input class="form-control" id="currency_symbol" type="text" placeholder="Enter ..." />
             </div>
        </div>
        
     </div>
     <div class="row">
     	<div class="col-md-6">
     		<div class="checkbox">
					<label>
					<input type="checkbox" id="is_active">
					 Is Active
					</label>
			</div>

     	</div>
     </div>
  
  </div><!-- /.box-body -->
  <div class="box-footer">
      <button id="btnSaveCurrency" class="btn btn-info pull-right" type="submit">Save Currency</button>
  </div><!-- box-footer -->
</div><!-- /.box -->
  
@stop


@section('javascript')
   <script type="text/javascript">
      $(document).ready(function(){

      	$("#btnSaveCurrency").click(function(){

      		          var token =$("#token").val();
      	            var currency_name=$("#currency_name").val();
      	            var currency_symbol= $("#currency_symbol").val();
      	            /* tarnary logic */ 
      	            var is_active= $("#is_active").is(":checked") == true ? 1: 0;

      	           // console.log(data);
                       
      	                 	        
			          
			        $.ajax({
				           url: 'currencysave',
				           type: 'POST',
				           dataType: 'json',
				           data: {
                             '_token': token,
                             'currency_name':currency_name,
                             'currency_symbol':currency_symbol,
                             'is_active':is_active
				           },
				           success: function(data, textStatus, xhr) {
				             console.log(data);
				           

				             //jQuery("#journal_id").val(data.journal_id);
				             
				           },
				           error: function(xhr, textStatus, errorThrown) {
				             //called when there is an error
				           }
                    });

                	
	    });

    

      });
   </script>

@stop