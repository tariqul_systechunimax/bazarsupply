<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use App\Models\Ecom_currency;

class CurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return view('admin.currencies.index');
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function viewcurrency(){

        $currencies = Ecom_currency::all();


        return View('admin.currencies.list',["ecom_currencies"=>$currencies]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
        //$currency_name = $request->input('currency_name');
       // $currency_symbol= $request->input('currency_symbol');
        //$is_active=$request->input('is_active');

       // var_dump($request->all());


         $validator= Validator::make($request->all(),
              [
                     'currency_name'=>'required|unique:ecom_currencies',
                     'currency_symbol'=>'required',
                     'is_active'=>'required'
              ]

         );

         if($validator->fails()){
            $error_msgs = array();
            $error_msgs["output"]="error";
            foreach ($validator->messages()->all() as $msg) {
                 $error_msgs[]= $msg;
            }
            return response()->json($error_msgs);

         }else{
            $currency = new Ecom_currency;
            $currency->currency_name = $request->input('currency_name');
            $currency->currency_symbol = $request->input('currency_symbol');
            $currency->is_active = $request->input('is_active');
            $currency->save();
            $new_currency_id = $currency->id;
            return response()->json(array('output'=>'success','msg'=> 'Currency Information Saved Successfully','currency_id'=>$new_currency_id)); 

         }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $currency = Ecom_currency::where('id','=',$id)->firstOrFail();
        //var_dump($currency);

        return View('admin.currencies.edit')->with(['ecom_currencies'=> $currency]);
        //
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
      //unset($request->input('_token'));

      $updateCurrencies = Ecom_currency::find($request->input('id'));
      $updateCurrencies->currency_name = $request->input('currency_name');
      $updateCurrencies->currency_symbol = $request->input('currency_symbol');
      $updateCurrencies->is_active = $request->input('is_active');
      $updateCurrencies->update();
      //$updateCurrencies->update($request->all());

     // $updateCurrencies->fill($request->all())->save();

      //return redirect()->back();

      return View('admin.currencies.edit')->with(['ecom_currencies'=> $updateCurrencies]);

    
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        Ecom_currency::find($request->all()['id'])->delete();
        return response()->json(array('output'=>'success','msg'=> 'Currency Information delete Successfully')); 
    }
}
